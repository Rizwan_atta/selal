package com.selal.selalf.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.selal.selalf.R;

/**
 * Created by MULTI.PC on 6/10/2018.
 */

public class SelalContact extends AppCompatActivity {
    Button button;
    private EditText Name;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_selal);
        button=findViewById(R.id.send_btn);
        Name = (EditText) findViewById(R.id.editText);
        editText = (EditText) findViewById(R.id.editText_name);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Name.getText().toString().equals("")){


                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("message/rfc822");
                    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@selal.com"});
                    i.putExtra(Intent.EXTRA_SUBJECT, editText.getText().toString());
                    i.putExtra(Intent.EXTRA_TEXT   , Name.getText().toString());
                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(SelalContact.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(SelalContact.this, "please enter something", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
