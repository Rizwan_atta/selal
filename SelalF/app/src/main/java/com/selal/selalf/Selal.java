package com.selal.selalf;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;


public class Selal  extends Application {
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}